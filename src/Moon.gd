extends KinematicBody2D

export var mass: float = 0.0
var is_dragging: bool = false

func _process(delta: float) -> void:
	if is_dragging:
		position = get_global_mouse_position()

func _on_Moon_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			is_dragging = not is_dragging
