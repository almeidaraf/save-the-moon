extends Area2D

export (PackedScene) var Bullet
export (float) var rotation_speed = 0.0
export (float) var shot_rate = 0.0
var acc: float = 0.0

func _process(delta: float) -> void:
	if Input.is_action_just_pressed("shoot"):
		shoot()
	elif Input.is_action_pressed("shoot") and acc >= shot_rate:
		shoot()
	acc += delta

	var yaw_direction: int = Input.get_action_strength("yaw right") - Input.get_action_strength("yaw left")
	yaw(yaw_direction, delta * 1.1)

func yaw(direction: int, delta: float) -> void:
	rotation += direction * rotation_speed * delta
	if rotation < -PI / 2:
		rotation = -PI / 2
	if rotation > PI / 2:
		rotation = PI / 2

func shoot() -> void:
	$ShotAudio.play()
	var b: Node = Bullet.instance()
	b.init($Muzzle.global_transform)
	get_parent().add_child(b)
	acc = 0.0
