extends Area2D

export var initial_speed: = 0
export var ttl_seconds: float = 0
var velocity: Vector2

func _ready() -> void:
	$AnimationPlayer.play("fadeout")
	var timer: = Timer.new()
	timer.wait_time = ttl_seconds
	timer.connect("timeout", self, "on_timeout_complete")
	add_child(timer)
	timer.start()

func init(t: Transform2D) -> void:
	transform = t
	velocity = -initial_speed * t.y

func _physics_process(delta: float) -> void:
	for body in get_tree().get_nodes_in_group("astrobody"):
		var gravity_direction = (body.position - position).normalized()
		var d = global_position.distance_to(body.global_position)
		var acc = body.mass / (d * d)
		velocity += gravity_direction * acc
	rotation = velocity.angle() + (PI / 2)
	position +=  velocity * delta

func on_timeout_complete():
	queue_free()

func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()

func _on_Bullet_body_entered(body: Node) -> void:
	queue_free()

func _on_Bullet_area_entered(area: Area2D) -> void:
	queue_free()
