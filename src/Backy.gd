extends Node2D

func _on_weakness_area_entered(area: Area2D) -> void:
	$DeathAudio.play()
	$weakness.queue_free()
	$shield.queue_free()
	hide()

func _on_DeathAudio_finished() -> void:
	queue_free()
